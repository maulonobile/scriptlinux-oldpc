#!/bin/bash
#####################################################################
# DNS Speed test with dig
# Set your /etc/resolv.conf and add your favourite DNS service (i.e. 9.9.9.9 or 1.1.1.1 or 8.8.8.8)
# Set target website
####################################################################

while true; do dig $1 | grep time; sleep 2; done
